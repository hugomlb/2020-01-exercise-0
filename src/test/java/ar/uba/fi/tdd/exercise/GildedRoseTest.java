package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void aProductNameShouldBeSetProperly() {
			Item[] items = new Item[] { new Item("item", 0, 0) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat("item").isEqualTo(app.items[0].Name);
	}

	@Test
	public void aProductSellinof5ShouldBeSetProperly() {
		Item[] items = new Item[] { new Item("item", 5, 0) };
		GildedRose app = new GildedRose(items);
		assertThat(5).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void aProductQuealityof5ShouldBeSetProperly() {
		Item[] items = new Item[] { new Item("item", 5, 5) };
		GildedRose app = new GildedRose(items);
		assertThat(5).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aNonZeroProductQualityWithPositiveSellinShouldDecreseInOneWhenUpdated() {
		Item[] items = new Item[] { new Item("item", 5, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(4).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aPostiveProductSellinShouldDecreseInOneWhenUpdated() {
		Item[] items = new Item[] { new Item("item", 5, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(4).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void aProductSellinOfOneShouldDecreseInOneWhenUpdated() {
		Item[] items = new Item[] { new Item("item", 1, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void aProductSellinOfZeroShouldDecreseInOneWhenUpdated() {
		Item[] items = new Item[] { new Item("item", 0, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(-1).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void aNegativeProductSellinShouldDecreseInOneWhenUpdated() {
		Item[] items = new Item[] { new Item("item", -2, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(-3).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void aNonZeroProductsQualityShouldDecreseByTwoWhenSellinIsZeroAndUpdated() {
		Item[] items = new Item[] { new Item("item", 0, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(3).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aNonZeroProductsQualityShouldDecreseByTwoWhenSellinIsNegativeAndUpdated() {
		Item[] items = new Item[] { new Item("item", -1, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(3).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aZeroProductsQualityShouldNotDecreseWhenSellinIsPositiveAndUpdated() {
		Item[] items = new Item[] { new Item("item", 5, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aZeroProductsQualityShouldNotDecreseWhenSellinIsNegativeAndUpdated() {
		Item[] items = new Item[] { new Item("item", -1, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aZeroProductsQualityShouldNotDecreseWhenSellinIsZeroAndUpdated() {
		Item[] items = new Item[] { new Item("item", 0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aProductsQualityOfOneShouldBeSetToZeroWhenSellinIsZeroAndUpdated() {
		Item[] items = new Item[] { new Item("item", 0, 1) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void aProductsQualityOfOneShouldBeSetToZeroWhenSellinNegativeAndUpdated() {
		Item[] items = new Item[] { new Item("item", -1, 1) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}
}
