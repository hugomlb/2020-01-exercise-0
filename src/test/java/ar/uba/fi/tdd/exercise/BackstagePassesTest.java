package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class BackstagePassesTest {
    @Test
    public void backstagePassesShouldIncreseInOneWhenSellinIsHigherThanTen() {
        Item[] items = new Item[] { new Item("Backstage passes", 12, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(11).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesShouldIncreseInTwoWhenSellinIsTen() {
        Item[] items = new Item[] { new Item("Backstage passes", 10, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(12).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesShouldIncreseInTwoWhenSellinIsBetweenTenAndFiven() {
        Item[] items = new Item[] { new Item("Backstage passes", 8, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(12).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesShouldIncreseInThreeWhenSellinIsFive() {
        Item[] items = new Item[] { new Item("Backstage passes", 5, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(13).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesShouldIncreseInThreeWhenSellinLowerThanFive() {
        Item[] items = new Item[] { new Item("Backstage passes", 2, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(13).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldNotIncreseeWhenSellinIsHigherThanTenAndQualityIs50() {
        Item[] items = new Item[] { new Item("Backstage passes", 12, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldNotIncreseWhenSellinIsTenAndQualityIs50() {
        Item[] items = new Item[] { new Item("Backstage passes", 10, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldNotIncreseWhenSellinIsBetweenTenAndFivenAndQualityIs50() {
        Item[] items = new Item[] { new Item("Backstage passes", 8, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldIncreseWhenSellinIsFiveAndQualityIs50() {
        Item[] items = new Item[] { new Item("Backstage passes", 5, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldNotIncreseWhenSellinLowerThanFiveAndQualityIs50() {
        Item[] items = new Item[] { new Item("Backstage passes", 2, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldBeSetTo50WhenSellinIsTenAndQualityIs49() {
        Item[] items = new Item[] { new Item("Backstage passes", 10, 49) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldBeSetTo50WhenSellinIsSixAndQualityIs49() {
        Item[] items = new Item[] { new Item("Backstage passes", 6, 49) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldBeSetTo50WhenSellinIsFiveAndQualityIs48() {
        Item[] items = new Item[] { new Item("Backstage passes", 5, 48) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void backstagePassesQualityShouldBeSetToZeroWhenSellinIsZero() {
        Item[] items = new Item[] { new Item("Backstage passes", 0, 48) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aPostiveBackstagePassesSellinShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Backstage passes", 5, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(4).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aBackstagePassesSellinOfOneShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Backstage passes", 1, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aBackstagePassesSellinOfZeroShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Backstage passes", 0, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-1).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aNegativeBackstagePassesSellinShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[]{new Item("Backstage passes", -2, 5)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-3).isEqualTo(app.items[0].sellIn);
    }
}
