package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ConjuredItemTest {

    @Test
    public void aPostiveConjuredItemsSellinShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 5, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(4).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aConjuredItemsSellinOfOneShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 1, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aConjuredItemsSellinOfZeroShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 0, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-1).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aNegativeConjuredItemsSellinShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", -2, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-3).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void aNonZeroConjuredItemsQualityWithPositiveSellinShouldDecreseInTwoWhenUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 5, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(3).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aZeroConjuredItemsQualityShouldNotDecreseWhenSellinIsPositiveAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 5, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aConjuredItemsQualityOfOneShouldBeSetToZeroWhenSellinIsPositiveAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 5, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aNonZeroConjuredItemsQualityShouldDecreseByFourWhenSellinIsZeroAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 0, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(1).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aNonZeroConjuredItemsQualityShouldDecreseByFourWhenSellinIsNegativeAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", -1, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(1).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aZeroConjuredItemsQualityShouldNotDecreseWhenSellinIsZeroAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aZeroConjuredItemsQualityShouldNotDecreseWhenSellinIsNegativeAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", -1, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aConjuredItemsQualityOfOneShouldBeSetToZeroWhenSellinIsZeroAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 0, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aConjuredItemsQualityOfThreeShouldBeSetToZeroWhenSellinIsZeroAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", 0, 3) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aConjuredItemsQualityOfOneShouldBeSetToZeroWhenSellinIsNegativeAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", -1, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }

    @Test
    public void aConjuredItemsQualityOfThreeShouldBeSetToZeroWhenSellinIsNegativeAndUpdated() {
        Item[] items = new Item[] { new Item("Conjured Item", -1, 3) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].quality);
    }
}
