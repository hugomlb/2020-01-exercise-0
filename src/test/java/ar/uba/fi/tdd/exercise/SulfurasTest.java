package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SulfurasTest {
    @Test
    public void sulfurasQualityShouldNotDecreseWhenUpdatedWithPositiveSellin() {
        Item[] items = new Item[] { new Item("Sulfuras", 5, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(80).isEqualTo(app.items[0].quality);
    }

    @Test
    public void sulfurasQualityShouldNotDecreseWhenUpdatedWithZeroSellin() {
        Item[] items = new Item[] { new Item("Sulfuras", 0, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(80).isEqualTo(app.items[0].quality);
    }

    @Test
    public void sulfurasQualityShouldNotDecreseWhenUpdatedWithNegativeSellin() {
        Item[] items = new Item[] { new Item("Sulfuras", -4, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(80).isEqualTo(app.items[0].quality);
    }

    @Test
    public void sulfurasSellinShouldNotDecreseWhenUpdatedWithPositiveSellin() {
        Item[] items = new Item[] { new Item("Sulfuras", 5, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(5).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void sulfurasSellinShouldNotDecreseWhenUpdatedWithZeroSellin() {
        Item[] items = new Item[] { new Item("Sulfuras", 0, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void sulfurasSellinShouldNotDecreseWhenUpdatedWithNegativeSellin() {
        Item[] items = new Item[] { new Item("Sulfuras", -4, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-4).isEqualTo(app.items[0].sellIn);
    }
}
