package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AgedBrieTest {

    @Test
    public void anAgedBriePositiveSellinShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Aged Brie", 5, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(4).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void anAgedBrieWithSellinOfOneShouldBeSetToZeroWhenUpdated() {
        Item[] items = new Item[] { new Item("Aged Brie", 1, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(0).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void anAgedBrieWithSellinOfZeroShouldBeSetToMinusONeWhenUpdated() {
        Item[] items = new Item[] { new Item("Aged Brie", 0, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-1).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void anAgedBrieNegativeSellinShouldDecreseInOneWhenUpdated() {
        Item[] items = new Item[] { new Item("Aged Brie", -1, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(-2).isEqualTo(app.items[0].sellIn);
    }

    @Test
    public void agedBrieQualityShouldIncreseByOneWhenUpdatedWithPossitiveSellin() {
        Item[] items = new Item[] { new Item("Aged Brie", 5, 1) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(4).isEqualTo(app.items[0].sellIn);
        assertThat(2).isEqualTo(app.items[0].quality);
    }

    @Test
    public void agedBrieQualityShouldIncreseByTwoWhenUpdatedWithNegativeSellin() {
        Item[] items = new Item[] { new Item("Aged Brie", -5, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(7).isEqualTo(app.items[0].quality);
    }

    @Test
    public void agedBrieQualityShouldNotIncreseWhenUpdatedWithPossitiveSellinIfQualityIs50() {
        Item[] items = new Item[] { new Item("Aged Brie", 5, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(4).isEqualTo(app.items[0].sellIn);
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void agedBrieQualityShouldNotIncreseWhenUpdatedWithNegativeSellinIfQualityIs50() {
        Item[] items = new Item[] { new Item("Aged Brie", -5, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }

    @Test
    public void agedBrieQualityShouldBeSetTo50WhenUpdatedWithNegativeSellinIfQualityIs49() {
        Item[] items = new Item[] { new Item("Aged Brie", -5, 49) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(50).isEqualTo(app.items[0].quality);
    }
}
