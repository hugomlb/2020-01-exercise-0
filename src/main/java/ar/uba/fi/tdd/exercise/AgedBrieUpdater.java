package ar.uba.fi.tdd.exercise;

public class AgedBrieUpdater extends Updater{

    public AgedBrieUpdater(){
    }

    @Override
    public void update(Item item) {
        update(item, qualityDegrader.increaseQuality, qualityDegrader.increaseQuality);
    }
}
