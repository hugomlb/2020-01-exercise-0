package ar.uba.fi.tdd.exercise;

public interface ItemFunction {
    void run(Item item);
}
