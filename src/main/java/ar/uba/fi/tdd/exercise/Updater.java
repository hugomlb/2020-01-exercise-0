package ar.uba.fi.tdd.exercise;

public abstract class Updater {

    static final int NORMAL_DEGRADER_SPEED = 1;
    static final int SELLIN_PASSED = -1;
    static final int SELLIN_DEGRADE_SPEED = 1;

    protected
        QualityDegrader qualityDegrader;

    public Updater() {
        qualityDegrader = new QualityDegrader(NORMAL_DEGRADER_SPEED);
    }

    public abstract void update(Item item);

    private void decreaseSellin(Item item) {
        item.sellIn = item.sellIn - SELLIN_DEGRADE_SPEED;
    }

    protected void ifItemsSellinIsLessOrEqualThanXThenExecuteFunction(Item item, int x, ItemFunction function) {
        if (item.sellIn <= x) {
            function.run(item);
        }
    }

    protected void update(Item item, ItemFunction mustAction, ItemFunction sellinPaseedAction) {
        mustAction.run(item);
        decreaseSellin(item);
        ifItemsSellinIsLessOrEqualThanXThenExecuteFunction(item, SELLIN_PASSED, sellinPaseedAction);
    }
}
