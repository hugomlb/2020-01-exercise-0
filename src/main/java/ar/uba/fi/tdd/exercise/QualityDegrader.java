package ar.uba.fi.tdd.exercise;

public class QualityDegrader {

    static final int HIGHEST_VALID_QUALITY = 50;
    static final int LOWEST_VALID_QUALITY = 0;

    public
        ItemFunction increaseQuality;
        ItemFunction decreaseQuality;
        ItemFunction setQualityToLowest;

    private final
        int degradeSpeed;

    public QualityDegrader(int speed){
        degradeSpeed = speed;
        increaseQuality = (anItem) -> {
            for (int i = 0; i < degradeSpeed; i++) {
                if (anItem.quality < HIGHEST_VALID_QUALITY) {
                    anItem.quality = anItem.quality + 1;
                }
            }
        };
        decreaseQuality = (anItem) -> {
            for (int i = 0; i < degradeSpeed; i++) {
                if (anItem.quality > LOWEST_VALID_QUALITY) {
                    anItem.quality = anItem.quality - 1;
                }
            }
        };
        setQualityToLowest = (anItem) -> {anItem.quality = LOWEST_VALID_QUALITY;};
    }
}
