package ar.uba.fi.tdd.exercise;

public class UpdaterFactory {

    public UpdaterFactory(){
    }

    public Updater buildUpdater(String name){
        Updater updater;
        switch (name) {
            case "Aged Brie":
                updater = new AgedBrieUpdater();
                break;
            case "Backstage passes":
                updater = new BackstagePassesUpdater();
                break;
            case "Sulfuras":
                updater = new SulfurasUpdater();
                break;
            case "Conjured Item":
                updater =  new ConjuredItemUpdater();
                break;
            default:
                updater = new CommonItemUpdater();
                break;
        }
        return updater;
    }
}
