
package ar.uba.fi.tdd.exercise;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    public void updateQuality() {
        UpdaterFactory updaterFactory = new UpdaterFactory();
        for (int i = 0; i < items.length; i++) {
            Updater updater = updaterFactory.buildUpdater(items[i].Name);
            updater.update(items[i]);
        }
    }
}
