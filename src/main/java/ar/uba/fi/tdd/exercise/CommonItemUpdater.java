package ar.uba.fi.tdd.exercise;

public class CommonItemUpdater extends  Updater{

    public CommonItemUpdater(){
    }

    @Override
    public void update(Item item) {
        update(item, qualityDegrader.decreaseQuality, qualityDegrader.decreaseQuality);
    }
}
