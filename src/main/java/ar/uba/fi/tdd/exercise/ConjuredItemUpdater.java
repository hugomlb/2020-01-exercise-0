package ar.uba.fi.tdd.exercise;

public class ConjuredItemUpdater extends Updater{

    static final int DEGRADER_SPEED = 2;

    public ConjuredItemUpdater(){
        qualityDegrader = new QualityDegrader(DEGRADER_SPEED);
    }

    @Override
    public void update(Item item) {
        update(item, qualityDegrader.decreaseQuality, qualityDegrader.decreaseQuality);
    }
}
