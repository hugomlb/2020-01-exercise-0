package ar.uba.fi.tdd.exercise;

public class BackstagePassesUpdater extends  Updater{

    static final int DOUBLE_INCREASE_RANGE = 10;
    static final int TRIPLE_INCREASE_RANGE = 5;

    public BackstagePassesUpdater(){
    }

    @Override
    public void update(Item item) {
        ifItemsSellinIsLessOrEqualThanXThenExecuteFunction(item, DOUBLE_INCREASE_RANGE, qualityDegrader.increaseQuality);
        ifItemsSellinIsLessOrEqualThanXThenExecuteFunction(item, TRIPLE_INCREASE_RANGE, qualityDegrader.increaseQuality);
        update(item, qualityDegrader.increaseQuality, qualityDegrader.setQualityToLowest);;
    }
}
